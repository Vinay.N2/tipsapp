/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    colors: {
      frost: "rgba(255,255,255,0.15)",
      tyson: "#002554",
      success: "#2ecc71",
      primary: "#2980b9",
    },
    extend: {},
  },
  plugins: [],
};
