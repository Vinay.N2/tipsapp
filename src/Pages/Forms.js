import React from "react";
import { Container, FloatingLabel, Form } from "react-bootstrap";
import { CgArrowLeftO } from "react-icons/cg";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";

const Forms = () => {
  let navigate = useNavigate();
  return (
    <div className="bg-tyson p-5 w-full">
      <Container className="bg-white rounded-md p-5 text-center watermark relative">
        <div className="text-2xl mb-2">Leader Floor Walk Card</div>
        <div className="uppercase text-3xl font-bold">Safety</div>
        <hr />
        <div className="grid-container grid grid-cols-5 gap-4">
          <div className="item2 col-span-4 text-left questions">
            <span className="text-2xl font-bold md:text-xl">
              Questions
            </span>
            <hr />
            <div>Is the area clear of Distractions?</div>
            <div>Are Hazards clearly marked?</div>
            <div>Do you see safety is a priority?</div>
            <div>Are actions needed to mitigate?</div>
          </div>
          <div className="item1 col-span-1  ">
            <div className="flex justify-between font-bold text-2xl md:text-xl">
              <span className="lg:pl-11 md:pl-2">Yes</span>
              <span className="lg:pr-11 md:pr-2">No</span>
            </div>
            <hr />
            <div>
              <form className="flex justify-between h-[40px] mb-2">
                <input type="radio" name="q1" value="Yes" />
                <input type="radio" name="q1" value="No" />
              </form>
              <form className="flex justify-between h-[40px] mb-2">
                <input type="radio" name="q2" value="Yes" />
                <input type="radio" name="q2" value="No" />
              </form>
              <form className="flex justify-between h-[40px] mb-2">
                <input type="radio" name="q3" value="Yes" />
                <input type="radio" name="q3" value="No" />
              </form>
              <form className="flex justify-between h-[40px] mb-2">
                <input type="radio" name="q4" value="Yes" />
                <input type="radio" name="q4" value="No" />
              </form>
            </div>
          </div>
        </div>
        <hr />
        <div className="overallObservations">
          <p className="text-2xl font-bold text-left">
            Overall Observations
          </p>
          <FloatingLabel
            controlId="floatingTextarea"
            label="Comments"
            className="text-left"
          >
            <Form.Control
              as="textarea"
              className="text-left"
              placeholder="Enter your Observations Here"
              style={{ height: "200px" }}
            />
          </FloatingLabel>
        </div>
        <hr />
        <div className="overallObservations">
          <p className="text-2xl font-bold text-left">
            Feedback for Floor Managers
          </p>
          <FloatingLabel
            controlId="floatingTextarea"
            label="Comments"
            className="text-left"
          >
            <Form.Control
              as="textarea"
              className="text-left"
              placeholder="Enter your Observations Here"
              style={{ height: "200px" }}
            />
          </FloatingLabel>
        </div>
        <hr />
        <div className="valueStream text-left my-3 grid grid-cols-1">
          <div className="flex items-center gap-2 mb-3">
            <span className="font-bold" style={{ width: "120px" }}>
              Value Stream
            </span>
            <Form.Control
              type="text"
              placeholder="Value Stream"
              style={{ width: "250px" }}
            />
          </div>
          <div className="flex items-center gap-2">
            <span className="font-bold" style={{ width: "120px" }}>
              Process
            </span>
            <Form.Control
              type="text"
              placeholder="Process"
              style={{ width: "250px" }}
            />
          </div>
        </div>
        <hr />
        <div className="leaderCoach text-left flex justify-between md:flex-col md:gap-3 my-4 ">
          <div className="flex items-center gap-2">
            <span className="font-bold" style={{ width: "120px" }}>
              Leader
            </span>
            <Form.Control
              type="text"
              placeholder="Leader"
              style={{ width: "250px" }}
            />
          </div>
          <div className="flex items-center gap-2">
            <span className="font-bold" style={{ width: "120px" }}>
              TIPS Coach
            </span>
            <Form.Control
              type="text"
              placeholder="TIPS Cocah"
              style={{ width: "250px" }}
            />
          </div>
          <div className="flex items-center gap-2">
            <span className="font-bold" style={{ width: "120px" }}>
              Date
            </span>
            <Form.Control
              type="date"
              placeholder="Date"
              style={{ width: "250px" }}
            />
          </div>
        </div>
        <div className="submitBtn flex justify-center mt-5">
          <Link
            className="bg-tyson px-5 py-2 text-white rounded-full text-xl flex items-center justify-center no-underline pb-3"
            to="/thankyou"
          >
            Submit
          </Link>
        </div>
        <button
          className="absolute top-5 left-8"
          onClick={() => {
            navigate("/");
          }}
        >
          <CgArrowLeftO className="text-4xl text-tyson" />
        </button>
      </Container>
    </div>
  );
};

export default Forms;
