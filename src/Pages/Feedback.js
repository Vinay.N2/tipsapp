import { Container } from "react-bootstrap";
import { RiFilter2Fill } from "react-icons/ri";
import { BsFillArrowRightCircleFill } from "react-icons/bs";
import { themes } from "../Data/thems";
import React from "react";
import { Link } from "react-router-dom";
import { feedBackData } from "../Data/feedbackData";

const Feedback = () => {
  return (
    <div className="h-fit w-full flex bg-tyson p-5">
      <Container className="bg-white rounded-md py-4 px-4 watermark h-full">
        <div className="feedBackandFilters flex justify-between items-center">
          <h1 className="lg:text-3xl md:text:2xl">Feedbacks</h1>
          <div className="filters flex">
            <div className="flex items-center text-base mr-6">
              <RiFilter2Fill />
              <select name="themes" id="themes">
                <option value={1}>Today</option>
                <option value={2}>Yesterday</option>
              </select>
            </div>
            <div className="flex items-center text-base">
              <RiFilter2Fill />
              <select name="themes" id="themes">
                <option value={0}>All</option>
                {themes.map((theme, i) => (
                  <option value={theme.id} key={i}>
                    {theme.short}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
        <hr />
        <div className="flex justify-center items-center">
          <div className="grid grid-cols-1 lg:grid-cols-2 grid-flow-row gap-4 items-center justify-center">
            {feedBackData.map((data, i) => (
              <Link
                to="/forms"
                className="flex bg-tyson rounded-md relative no-underline text-white md:w-[450px] w-[400px] "
                key={i}
              >
                <div className="no-underline flex rounded-md ">
                  <img src={data.img} alt="Safety" />
                  <div className="flex flex-col py-3 text-white">
                    <div className="text-xl">{data.title}</div>
                    <div className="text-sm mb-1">
                      Leader Floor Walk
                    </div>
                    <div className="text-xs">{data.date}</div>
                  </div>
                  <button className="text-xs flex items-center  justify-end w-[100%] absolute bottom-3 right-3 hover:underline">
                    View Feedback{" "}
                    <BsFillArrowRightCircleFill className="ml-1" />
                  </button>
                </div>
              </Link>
            ))}
          </div>
        </div>
      </Container>
    </div>
  );
};

export default Feedback;
