import React from "react";
import { Container } from "react-bootstrap";
import { IoCheckmarkCircleSharp } from "react-icons/io5";
import { Link } from "react-router-dom";
import { RiCameraLensFill } from "react-icons/ri";

const SubmittedPage = () => {
  return (
    <div className="bg-tyson flex h-100 w-100 p-6 ">
      <Container className="bg-white rounded-md h-50 p-8">
        <p className="text-3xl font-bold flex items-center pl-6">
          <IoCheckmarkCircleSharp className="text-success mr-2 text-4xl" />
          Thank you! Your Feedback has been Submitted!
        </p>
        <p className="pl-6">
          Please Navigate back to the Theme page to select a different
          theme!
        </p>
        <div>
          <Link
            to="/"
            className="ml-6 no-underline text-white items-center flex bg-tyson w-[200px] justify-center py-3 px-3 rounded-full"
          >
            <RiCameraLensFill className="mr-1 text-xl" />
            Pick a Theme
          </Link>
        </div>
      </Container>
    </div>
  );
};

export default SubmittedPage;
