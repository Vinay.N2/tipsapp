import React from "react";
import { Container } from "react-bootstrap";
import { themes } from "../Data/thems";
import { Link } from "react-router-dom";

const Home = () => {
  return (
    <div className="bg-[#002554] h-full w-full watermark1">
      <Container>
        <h1 className="text-white py-5">Pick a Theme</h1>
      </Container>
      <div className="flex justify-center items-center">
        <Container className="grid grid-cols-3 grid-flow-row gap-5 items-center justify-center m-5">
          {themes.map((theme, i) => (
            <div className="flex justify-center" key={i}>
              <Link
                to={`/forms/${theme.id}`}
                className="no-underline"
              >
                <div className="frostEffect rounded-md">
                  <img src={theme.img} alt="safety" width="120px" />
                  <p className="text-white text-lg text-center">
                    {theme.title}
                  </p>
                </div>
              </Link>
            </div>
          ))}
        </Container>
      </div>
    </div>
  );
};

export default Home;
