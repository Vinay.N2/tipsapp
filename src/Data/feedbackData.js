import Safety from "../Assets/safety.svg";
import Culture from "../Assets/culture.svg";
import Team from "../Assets/team.svg";
import Metrics from "../Assets/metrics.svg";
import Waste from "../Assets/waste.svg";
import CIP from "../Assets/cip.svg";
export const feedBackData = [
  {
    id: 1,
    title: "Safety",
    img: Safety,
    short: "Safety",
    date: "09 Sept 2022 | 3:30pm",
  },
  {
    id: 2,
    title: "TM Engagement",
    img: Team,
    short: "TME",
    date: "09 Sept 2022 | 3:15pm",
  },
  {
    id: 3,
    title: "Safety",
    img: Safety,
    short: "Safety",
    date: "09 Sept 2022 | 2:30pm",
  },
  {
    id: 4,
    title: "CIP",
    img: CIP,
    short: "CIP",
    date: "09 Sept 2022 | 2:00pm",
  },
  {
    id: 5,
    title: "CIP",
    img: CIP,
    short: "CIP",
    date: "09 Sept 2022 | 1:00pm",
  },
  {
    id: 6,
    title: "Culture",
    img: Culture,
    short: "Culture",
    date: "09 Sept 2022 | 12:20pm",
  },
  {
    id: 7,
    title: "OEE Metrics",
    img: Metrics,
    short: "Metrics",
    date: "09 Sept 2022 | 12:15pm",
  },
  {
    id: 8,
    title: "Waste",
    img: Waste,
    short: "Waste",
    date: "09 Sept 2022 | 12:00pm",
  },
];
