import Safety from "../Assets/safety.svg";
import Culture from "../Assets/culture.svg";
import Team from "../Assets/team.svg";
import Metrics from "../Assets/metrics.svg";
import Waste from "../Assets/waste.svg";
import CIP from "../Assets/cip.svg";
export const themes = [
  {
    id: 1,
    title: "Safety",
    img: Safety,
    short: "Safety",
  },
  {
    id: 2,
    title: "Culture",
    img: Culture,
    short: "Culture",
  },
  {
    id: 3,
    title: "TM Engagement",
    img: Team,
    short: "TME",
  },
  {
    id: 4,
    title: "OEE Metrics",
    img: Metrics,
    short: "Metrics",
  },
  {
    id: 5,
    title: "Waste",
    img: Waste,
    short: "Waste",
  },
  {
    id: 6,
    title: "Continuous Improvement Practices",
    img: CIP,
    short: "CIP",
  },
];
