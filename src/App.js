import "./App.css";
import { Routes, Route } from "react-router-dom";
import Home from "./Pages/Home";
import NavBar from "./components/Navbar/NavBar";
import Forms from "./Pages/Forms";
import Feedback from "./Pages/Feedback";
import SubmittedPage from "./Pages/SubmittedPage";
import Logo from "./Assets/logo1.png";
import Spinner from "react-bootstrap/Spinner";
import { useEffect, useState } from "react";

function App() {
  const [load, setLoad] = useState(false);
  useEffect(() => {
    setTimeout(() => {
      setLoad(true);
    }, 2500);
  }, []);
  return (
    <div className="App">
      {load ? (
        <>
          <NavBar />
          <div style={{ height: "60px" }}></div>
          <Routes>
            <Route path="/feedback" element={<Feedback />} />
            <Route path="/thankyou" element={<SubmittedPage />} />
            <Route path="/forms/:id" element={<Forms />} />
            <Route path="/" element={<Home />} />
          </Routes>
        </>
      ) : (
        <div className="bg-tyson flex flex-col h-screen w-full justify-center items-center">
          <div className="flex items-center mb-2">
            <img src={Logo} alt="Tyson Logo" width="80px" />
            <span className="tracking-widest pl-1 text-5xl font-mono text-white">
              IPS
            </span>
          </div>
          <Spinner animation="border" variant="light" />
        </div>
      )}
    </div>
  );
}

export default App;
