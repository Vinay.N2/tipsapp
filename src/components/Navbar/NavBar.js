import React from "react";
import { Link, NavLink } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Logo from "../../Assets/logo1.png";
import { TbClipboardCheck } from "react-icons/tb";
import { RiCameraLensFill } from "react-icons/ri";
const NavBar = () => {
  return (
    <div className="fixed w-full z-10">
      <Navbar collapseOnSelect bg="dark" variant="dark" expand="sm">
        <Container>
          <Navbar.Brand>
            <Link
              to="/"
              className="flex items-center text-white no-underline "
            >
              <img src={Logo} alt="Tyson Logo" width="40" />
              <span className="tracking-widest pl-1 text-2xl font-mono">
                IPS
              </span>
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle
            aria-controls="responsive-navbar-nav"
            aria-expanded="sm"
          />
          <Navbar.Collapse id="responsive-navbar-nav" expand="sm">
            <Nav className="me-auto"></Nav>
            <Nav>
              <NavLink
                to="/feedback"
                className="no-underline mr-6 text-white items-center flex"
              >
                <TbClipboardCheck className="mr-1 text-xl" />
                Feedback
              </NavLink>
              <NavLink
                to="/"
                className="no-underline text-white items-center flex"
              >
                <RiCameraLensFill className="mr-1 text-xl" />
                Pick a Theme
              </NavLink>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
};

export default NavBar;
